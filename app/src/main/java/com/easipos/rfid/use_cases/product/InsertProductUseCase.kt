package com.easipos.rfid.use_cases.product

import com.easipos.rfid.executor.PostExecutionThread
import com.easipos.rfid.executor.ThreadExecutor
import com.easipos.rfid.models.Product
import com.easipos.rfid.repositories.product.ProductRepository
import com.easipos.rfid.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import javax.inject.Inject

class InsertProductUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               private val repository: ProductRepository)
    : AbsRxObservableUseCase<Void, InsertProductUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Params): Observable<Void> =
            repository.insertProduct(params.product)

    class Params private constructor(val product: Product) {
        companion object {
            fun createQuery(product: Product): Params {
                return Params(product)
            }
        }
    }
}
