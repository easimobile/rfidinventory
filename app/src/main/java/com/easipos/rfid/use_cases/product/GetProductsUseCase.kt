package com.easipos.rfid.use_cases.product

import com.easipos.rfid.executor.PostExecutionThread
import com.easipos.rfid.executor.ThreadExecutor
import com.easipos.rfid.models.Product
import com.easipos.rfid.repositories.product.ProductRepository
import com.easipos.rfid.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetProductsUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                             postExecutionThread: PostExecutionThread,
                                             private val repository: ProductRepository)
    : AbsRxSingleUseCase<List<Product>, GetProductsUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<Product>> =
            repository.getProducts()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
