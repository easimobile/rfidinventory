package com.easipos.rfid.fragments.inventory

import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easipos.rfid.Easi
import com.easipos.rfid.R
import com.easipos.rfid.adapters.ProductAdapter
import com.easipos.rfid.base.CustomBaseFragment
import com.easipos.rfid.di.components.DaggerFragmentComponent
import com.easipos.rfid.di.components.FragmentComponent
import com.easipos.rfid.di.modules.FragmentModule
import com.easipos.rfid.fragments.inventory.mvp.InventoryPresenter
import com.easipos.rfid.fragments.inventory.mvp.InventoryView
import com.easipos.rfid.fragments.inventory.navigation.InventoryNavigation
import com.easipos.rfid.managers.PrinterManager
import com.easipos.rfid.models.Product
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.*
import org.jetbrains.anko.support.v4.findOptional
import java.nio.charset.Charset
import javax.inject.Inject

class InventoryFragment : CustomBaseFragment(), InventoryView, ProductAdapter.OnGestureDetectedListener {

    companion object {
        fun newInstance(): InventoryFragment {
            return InventoryFragment()
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: InventoryPresenter

    @Inject
    lateinit var navigation: InventoryNavigation

    @Inject
    lateinit var printerManager: PrinterManager

    private val printerConnectionTextView by lazy { findOptional<AppCompatTextView>(R.id.text_view_printer_connection) }
    private val connectPrinterButton by lazy { findOptional<AppCompatButton>(R.id.button_connect_printer) }
    private val addProductButton by lazy { findOptional<AppCompatButton>(R.id.button_add_product) }
    private val productRecyclerView by lazy { findOptional<RecyclerView>(R.id.recycler_view_product) }

    private var productAdapter: ProductAdapter? = null
    //endregion

    //region Lifecycle
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            init()
        }
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        getProducts()
        checkIsPrinterConnected()
    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun instantiateComponent(): FragmentComponent =
        DaggerFragmentComponent.builder()
            .appComponent((activity?.application as Easi).getAppComponent())
            .fragmentModule(FragmentModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.fragment_inventory

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
        setupListeners()
        setupProductRecyclerView()
        getProducts()
        connectPrinterIfAvailable()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun populateProducts(products: List<Product>) {
        productAdapter?.items = products.toMutableList()
    }
    //endregion

    //region Interface methods
    override fun onPrintProduct(product: Product) {
    }
    //endregion

    //region Action methods
    fun getProducts() {
        isAdded {
            presenter.doGetProducts()
        }
    }

    private fun setupListeners() {
        connectPrinterButton?.setOnClickListener {
            withActivity { activity ->
                navigation.navigateToConnectPrinter(activity)
            }
        }

        addProductButton?.setOnClickListener {
//            withActivity { activity ->
//                navigation.navigateToAddProduct(activity)
//            }
            AsyncTask.execute {
                try {
//                    val command = "^RB96,96\n" +
//                            "^FO65,50^A0N,30,30^FB900,2,,^FDNAME: [PROD_NM]^FS\n" +
//                            "^FO65,100^A0N,30,30^FB900,2,,^FDBRAND: [NAME]^FS\n" +
//                            "^FO65,150^A0N,30,30^FB900,2,,^FDDEPT: [DEPT_NM]^FS\n" +
//                            "^FO65,200^A0N,30,30^FDPRICE: [PRICE_1]^FS\n" +
//                            "^RFW,A^FD[EPC]^FS\n" +
//                            "^RFR,A,0,8,^FN1^FS^HV1,,^FS\n" +
//                            "^FO65,270^BY4^B3N,N,60,Y,N^FD[EPC]^FS\n" +
//                            "^FO385,200^IME:RFID.jpg^FS\n"
//                    printerManager.zebraPrinter?.storeFileOnPrinter(
//                        command.toByteArray(),
//                        "R:MOBILE_RFID.ZPL"
//                    )
//                    Logger.d(printerManager.zebraPrinter?.getObjectFromPrinter("R:MOBILE_RFID.ZPL")?.toString(
//                        Charset.defaultCharset()))
//                    Logger.d(PrinterUtil.listFiles("192.168.13.45", "*:*.*"))
//                    PrinterUtil.deleteFile("192.168.13.45", "R:MOBILE_RFID.*")
//                    PrinterUtil.sendContents("192.168.13.45", "^XA^DFR:MOBILE_RFID.ZPL^FO100,100^A0N,30,30^FDMOBILE_RFID.ZPL^FS^XZ")
//                printerManager.zebraPrinter?.sendCommand("^XA\n" +
//                        "^FO20,120^A0N,60^FN0^FS\n" +
//                        "^RI0,,5^FS\n" +
//                        "^HV0,,Tag ID:^FS\n" +
//                        "^XZ")
//                    printerManager.zebraPrinter?.sendCommand(command)
//                    printerManager.zebraPrinter?.printStoredFormat("R:MOBILE_RFID.ZPL", hashMapOf<Int, String>().apply {
//                        put(12, "Test first name")
//                        put(11, "Test last name")
//                    })

                    // Write UM
//                    val data = "^XA" +
//                            "^RS8" +
//                            "^RFW,A^FD00 my data^FS" +
//                            "^XZ"
//                    val output = printerManager.printerConnection?.sendAndWaitForResponse(data.toByteArray(), 1500, 1500, null)?.toString(
//                        Charset.defaultCharset())

                    // Read EPC
                    val data1 = "^XA" +
                            "^RS8" +
                            "^RFR,H,0,,E" +
                            "^FN1^FS^HV1,,^FS" +
                            "^XZ"
                    val output1 = printerManager.printerConnection?.sendAndWaitForResponse(data1.toByteArray(), 1500, 1500, null)?.toString(
                        Charset.defaultCharset())
                    Logger.d(output1)

                    // Read UM
//                    val data2 = "^XA" +
//                            "^RS8" +
//                            "^RFR,A,0,,3" +
//                            "^FN1^FS^HV1,,^FS" +
//                            "^XZ"
//                    val output2 = printerManager.printerConnection?.sendAndWaitForResponse(data2.toByteArray(), 1500, 1500, null)?.toString(
//                        Charset.defaultCharset())
//                    Logger.d(output2)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun setupProductRecyclerView() {
        Logger.d("")
        withContext { context ->
            productAdapter = ProductAdapter(context, this)
            productRecyclerView?.apply {
                this.adapter = productAdapter
                this.layoutManager = LinearLayoutManager(context)
                this.addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
            }
        }
    }

    private fun checkIsPrinterConnected() {
        withContext { context ->
            if (printerManager.isPrinterConnected()) {
                printerConnectionTextView?.text = getString(R.string.label_printer_connected)
                printerConnectionTextView?.setTextColor(context.findColor(android.R.color.holo_green_light))
            } else {
                printerConnectionTextView?.text = getString(R.string.label_connecting_printer)
                printerConnectionTextView?.setTextColor(context.findColor(android.R.color.holo_red_light))
            }
        }
    }

    private fun connectPrinterIfAvailable() {
        printerManager.connectIfAvailable(onConnectSuccess = {
            checkIsPrinterConnected()
        }, onConnectFail = {
            checkIsPrinterConnected()
        })
    }

    private fun getTemplateData(): String? {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<template name=\"TemplateExample\" card_type=\"2\" card_thickness=\"30\" delete=\"no\">\n" +
                "  <fonts>\n" +
                "    <font id=\"1\" name=\"Arial\" size=\"12\" bold=\"no\" italic=\"no\" underline=\"no\"/>\n" +
                "    <font id=\"2\" name=\"Arial\" size=\"14\" bold=\"yes\" italic=\"yes\" underline=\"no\"/>\n" +
                "  </fonts>\n" +
                "  <sides>\n" +
                "    <side name=\"front\" orientation=\"landscape\" rotation=\"0\" k_mode=\"text\">\n" +
                "      <print_types>\n" +
                "        <print_type type=\"mono\">\n" +
                "          <text field=\"firstName\" font_id=\"1\" x=\"100\" y=\"100\" width=\"0\" height=\"0\" angle=\"0\" color=\"0x000000\"/>\n" +
                "          <text field=\"lastName\" font_id=\"2\" x=\"100\" y=\"150\" width=\"0\" height=\"0\" angle=\"0\" color=\"0x000000\"/>\n" +
                "          <line x1=\"100\" y1=\"235\" x2=\"260\" y2=\"235\" thickness=\"10\" color=\"0\"/>\n" +
                "          <barcode field=\"qrCode\" code=\"qrCode\" x=\"100\" y=\"270\" width=\"75\" height=\"75\" quiet_zone_width=\"0\"/>\n" +
                "        </print_type>\n" +
                "      </print_types>\n" +
                "    </side>\n" +
                "  </sides>\n" +
                "</template>"
    }

    private fun populateTemplateFieldData(templateFields: List<String>): Map<String, String?>? {
        val fieldData: MutableMap<String, String?> =
            HashMap()
        for (fieldName in templateFields) {
            var fieldValue = ""
            when (fieldName) {
                "firstName" -> {
                    fieldValue = "Bob"
                }
                "lastName" -> {
                    fieldValue = "Smith"
                }
                "qrCode" -> {
                    fieldValue = "https://www.zebra.com"
                }
            }
            if (fieldValue.isNotEmpty()) {
                if (!fieldData.containsKey(fieldName)) {
                    fieldData[fieldName] = fieldValue
                }
            }
        }
        return fieldData
    }
    //endregion
}