package com.easipos.rfid.fragments.inventory.navigation

import android.app.Activity

interface InventoryNavigation {

    fun navigateToConnectPrinter(activity: Activity)

    fun navigateToAddProduct(activity: Activity)
}