package com.easipos.rfid.fragments.inventory.mvp

import com.easipos.rfid.base.View
import com.easipos.rfid.models.Product

interface InventoryView : View {

    fun populateProducts(products: List<Product>)
}
