package com.easipos.rfid.fragments.inventory.navigation

import android.app.Activity
import android.view.View
import com.easipos.rfid.activities.main.MainActivity
import kotlinx.android.extensions.LayoutContainer

class InventoryNavigationImpl(override val containerView: View)
    : LayoutContainer, InventoryNavigation {

    override fun navigateToConnectPrinter(activity: Activity) {
        (activity as? MainActivity)?.navigateToConnectPrinter()
    }

    override fun navigateToAddProduct(activity: Activity) {
        (activity as? MainActivity)?.navigateToAddProduct()
    }
}