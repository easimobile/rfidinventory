package com.easipos.rfid.fragments.rfid.mvp

import com.easipos.rfid.base.Presenter
import javax.inject.Inject

class RfidPresenter @Inject constructor()
    : Presenter<RfidView>()
