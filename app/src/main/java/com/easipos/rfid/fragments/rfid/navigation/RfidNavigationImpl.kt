package com.easipos.rfid.fragments.rfid.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

class RfidNavigationImpl(override val containerView: View)
    : LayoutContainer, RfidNavigation