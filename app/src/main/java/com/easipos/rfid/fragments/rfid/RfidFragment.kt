package com.easipos.rfid.fragments.rfid

import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import com.easipos.rfid.Easi
import com.easipos.rfid.R
import com.easipos.rfid.base.CustomBaseFragment
import com.easipos.rfid.di.components.DaggerFragmentComponent
import com.easipos.rfid.di.components.FragmentComponent
import com.easipos.rfid.di.modules.FragmentModule
import com.easipos.rfid.fragments.rfid.mvp.RfidPresenter
import com.easipos.rfid.fragments.rfid.mvp.RfidView
import com.easipos.rfid.fragments.rfid.navigation.RfidNavigation
import com.easipos.rfid.tools.RFIDThread
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.longToast
import org.jetbrains.anko.support.v4.findOptional
import javax.inject.Inject

class RfidFragment : CustomBaseFragment(), RfidView, RFIDThread.OnRFIDThreadListener {

    companion object {
        fun newInstance(): RfidFragment {
            return RfidFragment()
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: RfidPresenter

    @Inject
    lateinit var navigation: RfidNavigation

    @Inject
    lateinit var rfidThread: RFIDThread

    private val scanButton by lazy { findOptional<AppCompatButton>(R.id.button_scan) }

    private var isStartScan = false
    //endregion

    //region Lifecycle
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            init()
        }
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        rfidThread.addListener(this)
    }

    override fun onPause() {
        rfidThread.removeListener(this)
        super.onPause()
    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun instantiateComponent(): FragmentComponent =
        DaggerFragmentComponent.builder()
            .appComponent((activity?.application as Easi).getAppComponent())
            .fragmentModule(FragmentModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.fragment_rfid

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Interface Methods
    override fun onTagRead(data: Array<String>?) {
        Logger.d(data)
    }
    //endregion

    //region Action methods
    private fun setupViews() {

    }

    private fun setupListeners() {
        scanButton?.setOnClickListener {
            isStartScan = !isStartScan

            if (isStartScan) {
                startScan()
                scanButton?.setText(R.string.action_stop_scan)
            } else {
                stopScan()
                scanButton?.setText(R.string.action_start_scan)
            }
        }
    }

    private fun startScan() {
        rfidThread.myLib?.startInventoryTag()
    }

    private fun stopScan() {
        rfidThread.myLib?.stopInventory()
    }
    //endregion
}