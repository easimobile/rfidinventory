package com.easipos.rfid.fragments.inventory.mvp

import com.easipos.rfid.base.Presenter
import com.easipos.rfid.models.Product
import com.easipos.rfid.use_cases.base.DefaultSingleObserver
import com.easipos.rfid.use_cases.product.GetProductsUseCase
import javax.inject.Inject

class InventoryPresenter @Inject constructor(private val getProductsUseCase: GetProductsUseCase)
    : Presenter<InventoryView>() {

    override fun onDetachView() {
        super.onDetachView()
        getProductsUseCase.dispose()
    }

    fun doGetProducts() {
        getProductsUseCase.execute(object : DefaultSingleObserver<List<Product>>() {
            override fun onSuccess(value: List<Product>) {
                view?.populateProducts(value)
            }
        }, GetProductsUseCase.Params.createQuery())
    }
}
