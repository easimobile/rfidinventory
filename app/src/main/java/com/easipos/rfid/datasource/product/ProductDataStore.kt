package com.easipos.rfid.datasource.product

import com.easipos.rfid.models.Product
import io.reactivex.Observable
import io.reactivex.Single

interface ProductDataStore {

    fun insertProduct(product: Product): Observable<Void>

    fun getProducts(): Single<List<Product>>
}
