package com.easipos.rfid.datasource

import android.app.Application
import com.easipos.rfid.api.services.ApiService
import com.easipos.rfid.datasource.precheck.PrecheckDataSource
import com.easipos.rfid.datasource.precheck.PrecheckDataStore
import com.easipos.rfid.datasource.product.ProductDataSource
import com.easipos.rfid.datasource.product.ProductDataStore
import com.easipos.rfid.executor.PostExecutionThread
import com.easipos.rfid.executor.ThreadExecutor
import com.easipos.rfid.room.RoomService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataFactory @Inject constructor(private val application: Application,
                                      private val api: ApiService,
                                      private val roomService: RoomService,
                                      private val threadExecutor: ThreadExecutor,
                                      private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createProductDataSource(): ProductDataStore =
        ProductDataSource(api, roomService, threadExecutor, postExecutionThread)
}
