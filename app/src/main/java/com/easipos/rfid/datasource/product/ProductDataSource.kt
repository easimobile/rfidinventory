package com.easipos.rfid.datasource.product

import android.os.AsyncTask
import com.easipos.rfid.api.services.ApiService
import com.easipos.rfid.executor.PostExecutionThread
import com.easipos.rfid.executor.ThreadExecutor
import com.easipos.rfid.models.Product
import com.easipos.rfid.room.RoomService
import com.easipos.rfid.use_cases.complete
import com.easipos.rfid.use_cases.success
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductDataSource @Inject constructor(private val api: ApiService,
                                            private val roomService: RoomService,
                                            private val threadExecutor: ThreadExecutor,
                                            private val postExecutionThread: PostExecutionThread) : ProductDataStore {

    override fun insertProduct(product: Product): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.productDao().apply {
                    this.insertProduct(product)

                    emitter.complete()
                }
            }
        }

    override fun getProducts(): Single<List<Product>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.productDao().apply {
                    emitter.success(this.findProducts())
                }
            }
        }
}
