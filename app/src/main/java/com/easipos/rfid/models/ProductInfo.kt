package com.easipos.rfid.models

data class ProductInfo(
    val code: String,
    val tid: String
)