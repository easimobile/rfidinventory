package com.easipos.rfid.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import io.github.anderscheow.library.kotlinExt.formatAmount

@Entity
data class Product(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val productCode: String,
    val productName: String,
    val brand: String,
    val department: String,
    val price: Double,
    val quantity: Int = 1,
    val isPrinted: Boolean = false
) {
    
    fun formatPrice(): String {
        return price.formatAmount()
    }

    fun enablePrint(): Boolean {
        return !isPrinted
    }
}