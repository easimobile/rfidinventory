package com.easipos.rfid.models

data class Auth(val token: String)