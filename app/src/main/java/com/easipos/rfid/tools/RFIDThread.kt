package com.easipos.rfid.tools

import realid.rfidlib.MyLib

class RFIDThread : Thread() {

    interface OnRFIDThreadListener {
        fun onTagRead(data: Array<String>?)
    }

    var myLib: MyLib? = null
        private set
    private val listeners = ArrayList<OnRFIDThreadListener>()

    private var isStart = false

    override fun run() {
        while (isStart) {
            myLib?.readTagFromBuffer()?.let { data ->
                listeners.forEach {
                    it.onTagRead(data)
                }
            }
        }
    }

    override fun start() {
        isStart = true
        super.start()
    }

    fun setMyLib(myLib: MyLib) {
        this.myLib = myLib
    }

    fun addListener(listener: OnRFIDThreadListener) {
        this.listeners.add(listener)
    }

    fun removeListener(listener: OnRFIDThreadListener) {
        this.listeners.remove(listener)
    }

    fun removeAllListeners() {
        this.listeners.clear()
    }

    fun onStart() {
        isStart = true
    }

    fun onStop() {
        isStart = false
    }
}