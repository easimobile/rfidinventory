package com.easipos.rfid.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.easipos.mobileordering.room.Converters
import com.easipos.rfid.models.Product

@Database(
        entities = [
            Product::class
        ],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RoomService : RoomDatabase() {

    abstract fun productDao(): ProductDao
}
