package com.easipos.rfid.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.easipos.rfid.models.Product

@Dao
interface ProductDao {

    //region CartItem
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: Product): Long

    @Query("SELECT * FROM Product")
    fun findProducts(): List<Product>

    @Query("UPDATE Product SET isPrinted = 1 WHERE id = :id")
    fun updateProductIsPrinted(id: Int)

    @Query("DELETE FROM Product")
    fun removeAllProducts()
}