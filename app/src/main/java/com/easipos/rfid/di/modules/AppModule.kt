package com.easipos.rfid.di.modules

import android.app.Application
import android.content.Context
import com.easipos.rfid.executor.*
import com.easipos.rfid.managers.PrinterManager
import com.easipos.rfid.repositories.precheck.PrecheckDataRepository
import com.easipos.rfid.repositories.precheck.PrecheckRepository
import com.easipos.rfid.repositories.product.ProductDataRepository
import com.easipos.rfid.repositories.product.ProductRepository
import com.easipos.rfid.tools.RFIDThread
import dagger.Module
import dagger.Provides
import io.github.anderscheow.validator.Validator
import realid.rfidlib.MyLib
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun application(): Application {
        return app
    }

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideValidator(context: Context): Validator {
        return Validator.with(context)
    }

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @Singleton
    fun provideDiskIOExecutor(IOExecutor: IOExecutor): DiskIOExecutor {
        return IOExecutor
    }

    @Provides
    @Singleton
    fun providePrecheckRepository(precheckDataRepository: PrecheckDataRepository): PrecheckRepository {
        return precheckDataRepository
    }

    @Provides
    @Singleton
    fun provideProductkRepository(productDataRepository: ProductDataRepository): ProductRepository {
        return productDataRepository
    }

    @Provides
    @Singleton
    fun provideMyLib(context: Context): MyLib {
        return MyLib(context)
    }

    @Provides
    @Singleton
    fun provideRFIDThread(): RFIDThread {
        return RFIDThread()
    }

    @Provides
    @Singleton
    fun providePrinterManager(): PrinterManager {
        return PrinterManager()
    }
}
