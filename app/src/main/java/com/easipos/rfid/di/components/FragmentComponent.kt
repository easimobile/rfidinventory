package com.easipos.rfid.di.components

import com.easipos.rfid.di.modules.FragmentModule
import com.easipos.rfid.di.scope.PerFragment
import com.easipos.rfid.fragments.inventory.InventoryFragment
import com.easipos.rfid.fragments.rfid.RfidFragment
import dagger.Component

@PerFragment
@Component(dependencies = [(AppComponent::class)], modules = [FragmentModule::class])
interface FragmentComponent {

    fun inject(inventoryFragment: InventoryFragment)

    fun inject(rfidFragment: RfidFragment)
}
