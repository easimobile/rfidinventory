package com.easipos.rfid.di.components

import android.app.Application
import android.content.Context
import com.easipos.rfid.api.services.Api
import com.easipos.rfid.di.modules.ApiModule
import com.easipos.rfid.di.modules.AppModule
import com.easipos.rfid.di.modules.DatabaseModule
import com.easipos.rfid.executor.DiskIOExecutor
import com.easipos.rfid.executor.PostExecutionThread
import com.easipos.rfid.executor.ThreadExecutor
import com.easipos.rfid.managers.PrinterManager
import com.easipos.rfid.repositories.precheck.PrecheckRepository
import com.easipos.rfid.repositories.product.ProductRepository
import com.easipos.rfid.room.RoomService
import com.easipos.rfid.tools.RFIDThread
import dagger.Component
import io.github.anderscheow.validator.Validator
import realid.rfidlib.MyLib
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class, DatabaseModule::class])
interface AppComponent {

    fun inject(app: Application)

    val app: Application

    fun context(): Context

    fun validator(): Validator

    fun roomService(): RoomService

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun diskIoExecutor(): DiskIOExecutor

    fun api(): Api

    fun precheckRepository(): PrecheckRepository

    fun productRepository(): ProductRepository

    fun myLib(): MyLib

    fun rfidThread(): RFIDThread

    fun printerManager(): PrinterManager
}
