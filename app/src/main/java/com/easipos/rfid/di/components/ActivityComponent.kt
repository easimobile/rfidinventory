package com.easipos.rfid.di.components

import com.easipos.rfid.activities.add_product.AddProductActivity
import com.easipos.rfid.activities.connect_printer.ConnectPrinterActivity
import com.easipos.rfid.activities.main.MainActivity
import com.easipos.rfid.activities.splash.SplashActivity
import com.easipos.rfid.di.modules.ActivityModule
import com.easipos.rfid.di.scope.PerActivity
import dagger.Component

@PerActivity
@Component(dependencies = [(AppComponent::class)], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(splashActivity: SplashActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(connectPrinterActivity: ConnectPrinterActivity)

    fun inject(addProductActivity: AddProductActivity)
}
