package com.easipos.rfid.di.modules

import androidx.room.Room
import com.easipos.rfid.Easi
import com.easipos.rfid.room.RoomService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private val app: Easi,
                     private val dbName: String) {

    @Provides
    @Singleton
    fun provideRoomService(): RoomService {
        return Room.databaseBuilder(
            app.applicationContext, RoomService::class.java, dbName)
            .fallbackToDestructiveMigration()
            .build()
    }
}
