package com.easipos.rfid.di.modules

import androidx.fragment.app.Fragment
import com.easipos.rfid.fragments.inventory.navigation.InventoryNavigation
import com.easipos.rfid.fragments.inventory.navigation.InventoryNavigationImpl
import com.easipos.rfid.fragments.rfid.navigation.RfidNavigation
import com.easipos.rfid.fragments.rfid.navigation.RfidNavigationImpl
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    fun provideInventoryNavigation(): InventoryNavigation {
        return InventoryNavigationImpl(fragment.activity!!.window.decorView.rootView)
    }

    @Provides
    fun provideRfidNavigation(): RfidNavigation {
        return RfidNavigationImpl(fragment.activity!!.window.decorView.rootView)
    }
}
