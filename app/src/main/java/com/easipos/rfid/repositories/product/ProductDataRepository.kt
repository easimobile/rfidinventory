package com.easipos.rfid.repositories.product

import com.easipos.rfid.datasource.DataFactory
import com.easipos.rfid.models.Product
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProductDataRepository @Inject constructor(private val dataFactory: DataFactory) : ProductRepository {

    override fun insertProduct(product: Product): Observable<Void> =
        dataFactory.createProductDataSource()
            .insertProduct(product)

    override fun getProducts(): Single<List<Product>> =
        dataFactory.createProductDataSource()
            .getProducts()
}
