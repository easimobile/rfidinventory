package com.easipos.rfid.repositories.precheck

import com.easipos.rfid.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckRepository {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
