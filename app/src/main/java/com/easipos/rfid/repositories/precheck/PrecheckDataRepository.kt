package com.easipos.rfid.repositories.precheck

import com.easipos.rfid.api.requests.precheck.CheckVersionRequestModel
import com.easipos.rfid.datasource.DataFactory
import io.reactivex.Single
import javax.inject.Inject

class PrecheckDataRepository @Inject constructor(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
