package com.easipos.rfid.managers

import android.os.AsyncTask
import com.easipos.rfid.tools.Preference
import com.zebra.sdk.comm.Connection
import com.zebra.sdk.comm.TcpConnection
import com.zebra.sdk.printer.ZebraPrinterFactory
import com.zebra.sdk.printer.ZebraPrinterLinkOs

class PrinterManager {

    var printerConnection: Connection? = null
        private set
    var zebraPrinter: ZebraPrinterLinkOs? = null
        private set

    fun connectIfAvailable(onConnectSuccess: () -> Unit, onConnectFail: (Exception) -> Unit) {
        if (Preference.prefPrinterIp.isNotBlank() && Preference.prefPrinterPort != 0) {
            connect(Preference.prefPrinterIp, Preference.prefPrinterPort, onConnectSuccess, onConnectFail)
        }
    }

    fun connect(ipAddress: String, port: Int, onConnectSuccess: () -> Unit, onConnectFail: (Exception) -> Unit) {
        AsyncTask.execute {
            printerConnection = TcpConnection(ipAddress, port)
            Preference.prefPrinterIp = ipAddress
            Preference.prefPrinterPort = port

            try {
                printerConnection?.open()
                onConnectSuccess()

                connectZebraPrinter()
            } catch (ex: Exception) {
                disconnect()
                onConnectFail(ex)
            }
        }
    }

    fun disconnect() {
        try {
            printerConnection?.close()
        } catch (ex: Exception) {
            disconnect()
        }
    }

    fun isPrinterConnected(): Boolean {
        return printerConnection != null && printerConnection!!.isConnected
    }

    private fun connectZebraPrinter() {
        if (isPrinterConnected()) {
            zebraPrinter = ZebraPrinterFactory.getLinkOsPrinter(printerConnection!!)
        }
    }
}