package com.easipos.rfid.api.services

import com.easipos.rfid.api.misc.ResponseModel
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiService @Inject constructor(private val api: Api) : Api {

    override fun checkVersion(body: RequestBody): Single<ResponseModel<Boolean>> {
        return api.checkVersion(body)
    }

    override fun registerFcmToken(body: RequestBody): Completable {
        return api.registerFcmToken(body)
    }

    override fun removeFcmToken(body: RequestBody): Completable {
        return api.removeFcmToken(body)
    }
}
