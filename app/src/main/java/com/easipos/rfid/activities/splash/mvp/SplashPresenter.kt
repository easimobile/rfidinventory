package com.easipos.rfid.activities.splash.mvp

import com.easipos.rfid.api.requests.precheck.CheckVersionRequestModel
import com.easipos.rfid.base.Presenter
import com.easipos.rfid.managers.UserManager
import com.easipos.rfid.tools.Preference
import com.easipos.rfid.use_cases.base.DefaultSingleObserver
import com.easipos.rfid.use_cases.precheck.CheckVersionUseCase
import javax.inject.Inject

class SplashPresenter @Inject constructor(private val checkVersionUseCase: CheckVersionUseCase)
    : Presenter<SplashView>() {

    fun checkVersion() {
        checkVersionUseCase.execute(object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(value: Boolean) {
                super.onSuccess(value)
                if (value) {
                    view?.showUpdateAppDialog()
                } else {
                    checkIsAuthenticated()
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                checkIsAuthenticated()
            }
        }, CheckVersionUseCase.Params.createQuery(CheckVersionRequestModel()))
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}
