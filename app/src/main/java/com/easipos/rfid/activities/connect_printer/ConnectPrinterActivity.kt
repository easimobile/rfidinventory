package com.easipos.rfid.activities.connect_printer

import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.easipos.rfid.Easi
import com.easipos.rfid.R
import com.easipos.rfid.base.CustomBaseAppCompatActivity
import com.easipos.rfid.di.components.ActivityComponent
import com.easipos.rfid.di.components.DaggerActivityComponent
import com.easipos.rfid.di.modules.ActivityModule
import com.easipos.rfid.managers.PrinterManager
import com.easipos.rfid.tools.Preference
import com.zebra.sdk.comm.TcpConnection
import kotlinx.android.synthetic.main.activity_connect_printer.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

class ConnectPrinterActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ConnectPrinterActivity::class.java)
        }
    }

    //region Variables
    @Inject
    lateinit var printerManager: PrinterManager
    //endregion

    override fun getSupportParentActivityIntent(): Intent? {
        finish()
        return null
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_connect_printer

    override fun getToolbar(): Toolbar? = toolbar

    override fun requiredDisplayHomeAsUp(): Boolean = true

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
    }

    override fun init() {
        super.init()
        setupViews()
        setupListeners()
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        if (Preference.prefPrinterIp.isNotBlank()) {
            text_input_edit_text_ip_address.setText(Preference.prefPrinterIp)
        }

        if (Preference.prefPrinterPort != 0) {
            text_input_edit_text_port.setText(Preference.prefPrinterPort.toString())
        } else {
            text_input_edit_text_port.setText(TcpConnection.DEFAULT_ZPL_TCP_PORT.toString())
        }
    }

    private fun setupListeners() {
        button_connect_printer.setOnClickListener {
            connectPrinter()
        }
    }

    private fun connectPrinter() {
        val ipAddress = text_input_edit_text_ip_address.text?.toString() ?: ""
        val port = text_input_edit_text_port.text?.toString()?.toIntOrNull() ?: 0

        if (ipAddress.isNotBlank() && port != 0) {
            checkLoadingIndicator(true, 0)
            printerManager.connect(ipAddress, port, onConnectSuccess = {
                runOnUiThread {
                    checkLoadingIndicator(false, 0)
                    longToast("Printer connected")
                    finish()
                }
            }, onConnectFail = { ex ->
                runOnUiThread {
                    ex.printStackTrace()
                    checkLoadingIndicator(false, 0)
                    longToast("Failed to connect printer")
                }
            })
        }
    }
    //endregion
}
