package com.easipos.rfid.activities.splash

import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.easipos.rfid.Easi
import com.easipos.rfid.R
import com.easipos.rfid.activities.splash.mvp.SplashPresenter
import com.easipos.rfid.activities.splash.mvp.SplashView
import com.easipos.rfid.activities.splash.navigation.SplashNavigation
import com.easipos.rfid.base.CustomBaseAppCompatActivity
import com.easipos.rfid.bundle.ParcelData
import com.easipos.rfid.di.components.ActivityComponent
import com.easipos.rfid.di.components.DaggerActivityComponent
import com.easipos.rfid.di.modules.ActivityModule
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.rate
import javax.inject.Inject

class SplashActivity : CustomBaseAppCompatActivity(), SplashView {

    companion object {
        fun newIntent(context: Context, clearDb: Boolean = false): Intent {
            return Intent(context, SplashActivity::class.java).apply {
                this.putExtra(ParcelData.CLEAR_DB, clearDb)
            }
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: SplashPresenter

    @Inject
    lateinit var navigation: SplashNavigation

    private val clearDb by argument(ParcelData.CLEAR_DB, false)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_splash

    override fun getToolbar(): Toolbar? = null

    override fun getEventBusType(): EventBusType? = null

    override fun requiredDisplayHomeAsUp(): Boolean = false

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
        super.init()
        checkVersion()
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
    }

    override fun navigateToMain() {
    }
    //endregion

    //region Action Methods
    private fun checkVersion() {
        presenter.checkVersion()
    }
    //endregion
}
