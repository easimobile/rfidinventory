package com.easipos.rfid.activities.main.navigation

import android.app.Activity

interface MainNavigation {

    fun navigateToConnectPrinter(activity: Activity)

    fun navigateToAddProduct(activity: Activity)
}