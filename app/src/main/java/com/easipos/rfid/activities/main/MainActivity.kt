package com.easipos.rfid.activities.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.easipos.rfid.Easi
import com.easipos.rfid.R
import com.easipos.rfid.activities.main.mvp.MainPresenter
import com.easipos.rfid.activities.main.mvp.MainView
import com.easipos.rfid.activities.main.navigation.MainNavigation
import com.easipos.rfid.base.CustomBaseAppCompatActivity
import com.easipos.rfid.di.components.ActivityComponent
import com.easipos.rfid.di.components.DaggerActivityComponent
import com.easipos.rfid.di.modules.ActivityModule
import com.easipos.rfid.fragments.inventory.InventoryFragment
import com.easipos.rfid.fragments.rfid.RfidFragment
import com.easipos.rfid.tools.RFIDThread
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.findColor
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import realid.rfidlib.EmshConstant
import realid.rfidlib.MyLib
import java.util.*
import javax.inject.Inject

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var navigation: MainNavigation

    @Inject
    lateinit var myLib: MyLib

    @Inject
    lateinit var rfidThread: RFIDThread

    private val inventoryFragment by lazy { InventoryFragment.newInstance() }
    private val rfidFragment by lazy { RfidFragment.newInstance() }

    private val tabFragments = arrayListOf<Fragment>()

    private var mTimer: Timer? = null
    private var mTimerTask: TimerTask? = null

    private var mEmshStatusReceiver: EmshStatusBroadcastReceiver? = null
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        monitorEmsh()
    }

    override fun onStart() {
        super.onStart()
        rfidThread.onStart()
    }

    override fun onStop() {
        rfidThread.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        releaseResources()
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_main

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
        super.init()
        setupRFIDThread()
        setupTab()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Navigation Methods
    fun navigateToConnectPrinter() {
        navigation.navigateToConnectPrinter(this)
    }

    fun navigateToAddProduct() {
        navigation.navigateToAddProduct(this)
    }
    //endregion

    //region Action Methods
    private fun setupRFIDThread() {
        rfidThread.setMyLib(myLib)

        if (myLib.powerOn()) {
            rfidThread.start()
        }
    }

    private fun monitorEmsh() {
        releaseResources()

        mEmshStatusReceiver = EmshStatusBroadcastReceiver()
        val intentFilter = IntentFilter(EmshConstant.Action.INTENT_EMSH_BROADCAST)
        registerReceiver(mEmshStatusReceiver, intentFilter)

        mTimer = Timer()
        mTimerTask = object : TimerTask() {
            override fun run() {
                val intent = Intent(EmshConstant.Action.INTENT_EMSH_REQUEST)
                intent.putExtra(
                    EmshConstant.IntentExtra.EXTRA_COMMAND,
                    EmshConstant.Command.CMD_REFRESH_EMSH_STATUS
                )
                sendBroadcast(intent)
            }
        }
        mTimer?.schedule(mTimerTask, 0, 1000)
    }

    private fun releaseResources() {
        mEmshStatusReceiver?.let { receiver ->
            unregisterReceiver(receiver)
        }
        mTimerTask?.cancel()
        mTimerTask = null
        mTimer?.cancel()
        mTimer = null
    }

    private fun setupTab() {
        tabFragments.apply {
            this.add(inventoryFragment)
            this.add(rfidFragment)
        }

        with(bottom_nav_view) {
            this.addItem(AHBottomNavigationItem(getString(R.string.nav_inventory), 0))
            this.addItem(AHBottomNavigationItem(getString(R.string.nav_rfid), 0))

            this.isForceTint = false
            this.defaultBackgroundColor = findColor(android.R.color.white)
            this.accentColor = findColor(R.color.colorTabActive)
            this.inactiveColor = findColor(R.color.colorTabInactive)
            this.isBehaviorTranslationEnabled = false
            this.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
            this.setOnTabSelectedListener { position, _ ->
                view_pager.setCurrentItem(position, false)
                true
            }
        }

        setupViewPager()
    }

    private fun setupViewPager() {
        val adapter = MainFragmentPagerAdapter(supportFragmentManager)
        view_pager.adapter = adapter
        view_pager.offscreenPageLimit = 10
    }
    //endregion

    private inner class EmshStatusBroadcastReceiver : BroadcastReceiver() {
        private var oldStatus = -1

        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null && intent.action == EmshConstant.Action.INTENT_EMSH_BROADCAST) {
                val sessionStatus = intent.getIntExtra("SessionStatus", 0)
                val batteryPowerMode = intent.getIntExtra("BatteryPowerMode", -1)
                if (sessionStatus and EmshConstant.EmshSessionStatus.EMSH_STATUS_POWER_STATUS != 0) {
                    if (batteryPowerMode == oldStatus) {
                        return
                    }
                    oldStatus = batteryPowerMode
                    when (batteryPowerMode) {
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_STANDBY -> {
                            Logger.d("Standby")
                            context?.toast("Standby")
                            myLib.powerOn()

                        }
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_DSG_UHF -> {
                            Logger.d("RFID Power On...")
                            context?.toast("RFID Power On...")

                            AsyncTask.execute {
                                Logger.d("Read Tag Mode: ${myLib.readTagModeGet()}")
                                Logger.d("Set Tag Mode: ${myLib.readTagModeSet(0, true)}")
                                Logger.d("Read Tag Mode: ${myLib.readTagModeGet()}")
                            }

                        }
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_CHG_GENERAL,
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_CHG_QUICK -> {
                            Logger.d("Charging...")
                            context?.toast("Charging...")
                        }
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_CHG_FULL -> {
                            Logger.d("Pistol Battery Full")
                            context?.toast("Pistol Battery Full")
                        }
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_BATTERY_LOW -> {
                            Logger.d("Pistol Battery Low")
                            context?.toast("Pistol Battery Low")
                        }
                        EmshConstant.EmshBatteryPowerMode.EMSH_PWR_MODE_BATTERY_ERROR -> {
                            Logger.d("Pistol Battery Error")
                            context?.toast("Pistol Battery Error")
                        }
                    }
                } else {
                    Logger.d("Warning")
                }
            }
        }
    }

    private inner class MainFragmentPagerAdapter internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment = tabFragments[position]

        override fun getCount(): Int = tabFragments.count()
    }
}
