package com.easipos.rfid.activities.add_product.mvp

import com.easipos.rfid.base.View

interface AddProductView : View {

    fun finishScreen()
}
