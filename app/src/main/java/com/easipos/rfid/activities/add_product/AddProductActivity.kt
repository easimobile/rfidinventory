package com.easipos.rfid.activities.add_product

import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.easipos.rfid.Easi
import com.easipos.rfid.R
import com.easipos.rfid.activities.add_product.mvp.AddProductPresenter
import com.easipos.rfid.activities.add_product.mvp.AddProductView
import com.easipos.rfid.base.CustomBaseAppCompatActivity
import com.easipos.rfid.di.components.ActivityComponent
import com.easipos.rfid.di.components.DaggerActivityComponent
import com.easipos.rfid.di.modules.ActivityModule
import com.easipos.rfid.models.Product
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_add_product.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

class AddProductActivity : CustomBaseAppCompatActivity(), AddProductView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, AddProductActivity::class.java)
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: AddProductPresenter

    @Inject
    lateinit var validator: Validator
    //endregion

    //region Lifecycle
    override fun getSupportParentActivityIntent(): Intent? {
        finish()
        return null
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_add_product

    override fun getToolbar(): Toolbar? = toolbar

    override fun requiredDisplayHomeAsUp(): Boolean = true

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
        super.init()
        setTitle(R.string.title_add_product)

        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Action Methods
    private fun setupListeners() {
        button_add_product.setOnClickListener {
            attemptAddProduct()
        }
    }

    private fun attemptAddProduct() {
        val productCodeValidation = Validation(text_input_layout_product_code)
            .add(NotBlankRule(R.string.error_field_required))
        val productNameValidation = Validation(text_input_layout_product_name)
            .add(NotBlankRule(R.string.error_field_required))
        val productBrandValidation = Validation(text_input_layout_product_brand)
            .add(NotBlankRule(R.string.error_field_required))
        val productDepartmentValidation = Validation(text_input_layout_product_department)
            .add(NotBlankRule(R.string.error_field_required))
        val productPriceValidation = Validation(text_input_layout_product_price)
            .add(NotBlankRule(R.string.error_field_required))
        val productQuantityValidation = Validation(text_input_layout_product_quantity)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val code = values[0]
                val name = values[1]
                val brand = values[2]
                val department = values[3]
                val price = values[4].toDoubleOrNull() ?: 0.0
                val quantity = values[5].toIntOrNull() ?: 1

                val product = Product(
                    productCode = code,
                    productName = name,
                    brand = brand,
                    department = department,
                    price = price,
                    quantity = quantity
                )

                presenter.doInsertProduct(product)
            }
        }).validate(productCodeValidation, productNameValidation, productBrandValidation,
            productDepartmentValidation, productPriceValidation, productQuantityValidation)
    }
    //endregion
}
