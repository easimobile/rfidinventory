package com.easipos.rfid.activities.main.navigation

import android.app.Activity
import android.view.View
import com.easipos.rfid.activities.add_product.AddProductActivity
import com.easipos.rfid.activities.connect_printer.ConnectPrinterActivity
import kotlinx.android.extensions.LayoutContainer

class MainNavigationImpl(override val containerView: View)
    : LayoutContainer, MainNavigation {

    override fun navigateToConnectPrinter(activity: Activity) {
        activity.startActivity(ConnectPrinterActivity.newIntent(activity))
    }

    override fun navigateToAddProduct(activity: Activity) {
        activity.startActivity(AddProductActivity.newIntent(activity))
    }
}