package com.easipos.rfid.activities.add_product.mvp

import com.easipos.rfid.R
import com.easipos.rfid.base.Presenter
import com.easipos.rfid.models.Product
import com.easipos.rfid.use_cases.base.DefaultObserver
import com.easipos.rfid.use_cases.product.InsertProductUseCase
import javax.inject.Inject

class AddProductPresenter @Inject constructor(private val insertProductUseCase: InsertProductUseCase)
    : Presenter<AddProductView>() {

    override fun onDetachView() {
        super.onDetachView()
        insertProductUseCase.dispose()
    }

    fun doInsertProduct(product: Product) {
        view?.setLoadingIndicator(true)
        insertProductUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)
                view?.toastMessage(R.string.prompt_add_product_successfully)
                view?.finishScreen()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.finishScreen()
            }
        }, InsertProductUseCase.Params.createQuery(product))
    }
}
