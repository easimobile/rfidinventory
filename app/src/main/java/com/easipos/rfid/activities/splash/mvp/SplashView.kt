package com.easipos.rfid.activities.splash.mvp

import com.easipos.rfid.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
