package com.easipos.rfid.activities.main.mvp

import com.easipos.rfid.base.Presenter
import javax.inject.Inject

class MainPresenter @Inject constructor()
    : Presenter<MainView>()
