package com.easipos.rfid.base

import android.content.Context
import com.easipos.rfid.Easi
import com.easipos.rfid.di.components.ActivityComponent
import io.github.anderscheow.library.appCompat.activity.LifecycleAppCompatActivity
import io.github.anderscheow.library.viewModel.BaseAndroidViewModel

abstract class CustomLifecycleActivity<VM : BaseAndroidViewModel<*>> : LifecycleAppCompatActivity<VM>() {

    var component: ActivityComponent? = null
        private set

    abstract fun instantiateComponent(): ActivityComponent

    open fun initInjection() {
        if (component == null) {
            component = instantiateComponent()
        }
    }

    override fun initBeforeSuperOnCreate() {
        initInjection()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(Easi.localeManager.setLocale(base))
    }
}
