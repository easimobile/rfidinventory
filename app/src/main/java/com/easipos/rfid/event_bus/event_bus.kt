package com.easipos.rfid.event_bus

data class NotificationCount(val count: Int)