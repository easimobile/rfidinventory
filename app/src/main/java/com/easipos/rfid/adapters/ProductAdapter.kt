package com.easipos.rfid.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.rfid.R
import com.easipos.rfid.databinding.ViewProductBinding
import com.easipos.rfid.models.Product
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product.*

class ProductAdapter(context: Context,
                     private val listener: OnGestureDetectedListener) : BaseRecyclerViewAdapter<Product>(context) {

    interface OnGestureDetectedListener {
        fun onPrintProduct(product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductBinding>(
            layoutInflater, R.layout.view_product, parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class ProductViewHolder(private val binding: ViewProductBinding)
        : BaseViewHolder<Product>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Product) {
            button_print.setOnClickListener {
                listener.onPrintProduct(item)
            }
        }

        override fun onClick(view: View, item: Product?) {
        }
    }
}